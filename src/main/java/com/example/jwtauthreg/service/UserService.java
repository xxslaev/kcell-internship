package com.example.jwtauthreg.service;

import com.example.jwtauthreg.Entity.RoleEntity;
import com.example.jwtauthreg.Entity.TaskEntity;
import com.example.jwtauthreg.Entity.UserEntity;
import com.example.jwtauthreg.repository.RoleEntityRepository;
import com.example.jwtauthreg.repository.TaskEntityRepository;
import com.example.jwtauthreg.repository.UserEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private TaskEntityRepository taskEntityRepository;

    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private RoleEntityRepository roleEntityRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserEntity saveUser(UserEntity userEntity) {
        RoleEntity userRole = roleEntityRepository.findByName("ROLE_USER");
        userEntity.setRoleEntity(userRole);
        userEntity.setPassword(passwordEncoder.encode(userEntity.getPassword()));
        return userEntityRepository.save(userEntity);
    }

    public UserEntity findByLogin(String login) {
        return userEntityRepository.findByLogin((login));
    }

    public UserEntity findByLoginAndPassword(String login, String password) {
        UserEntity userEntity = findByLogin(login);
        if (userEntity != null) {
            if (passwordEncoder.matches(password, userEntity.getPassword())) {
                return userEntity;
            }
        }
        return null;
    }

    public TaskEntity post(TaskEntity taskEntity){
        return taskEntityRepository.save(taskEntity);
    }

    public String del(TaskEntity taskEntity){
        taskEntityRepository.deleteById(taskEntity.getId());
        return "Задача успешно удалена :)";
    }

    public TaskEntity getTaskById(int id) {
        return taskEntityRepository.findById(id).orElse(null);
    }

    public List<TaskEntity> getTaskByDate(LocalDate date) {
        return taskEntityRepository.findAllByDate(date);
    }

    public List<TaskEntity> getTasks(){
        return taskEntityRepository.findAll();
    }

    public TaskEntity put(TaskEntity taskEntity){
        TaskEntity task = taskEntityRepository.findById(taskEntity.getId()).orElse(null);
        task.setProcess(taskEntity.getProcess());
        return taskEntityRepository.save(task);
    }
}

package com.example.jwtauthreg.Entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "user_table")
@Data
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true)
    private String login;

    @Column
    private String password;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private RoleEntity roleEntity;

    /*@OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private List<TaskEntity> taskEntities;*/
}

package com.example.jwtauthreg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtauthregApplication {

    public static void main(String[] args) {
        SpringApplication.run(JwtauthregApplication.class, args);
    }

}

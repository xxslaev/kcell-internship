package com.example.jwtauthreg.controller;

import com.example.jwtauthreg.Entity.TaskEntity;
import com.example.jwtauthreg.Entity.UserEntity;
import com.example.jwtauthreg.config.jwt.JwtProvider;
import com.example.jwtauthreg.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtProvider jwtProvider;

    @PostMapping("/register")
    public String registerUser(@RequestBody @Valid RegistrationRequest registrationRequest) {
        UserEntity u = new UserEntity();
        u.setPassword(registrationRequest.getPassword());
        u.setLogin(registrationRequest.getLogin());
        userService.saveUser(u);
        return "Поздравляю, вы успешно зарегестрировались :)";
    }

    @PostMapping("/auth")
    public AuthResponse authResponse(@RequestBody AuthRequest authRequest) {
        UserEntity userEntity = userService.findByLoginAndPassword(authRequest.getLogin(), authRequest.getPassword());
        String token = jwtProvider.generateToken(userEntity.getLogin());
        return new AuthResponse(token);
    }

    @PostMapping("/user/taskadd")
    public TaskEntity addTask(@RequestBody TaskEntity taskEntity){
        return userService.post(taskEntity);
    }

    @DeleteMapping("/user/taskdelete")
    public String delTask(@RequestBody TaskEntity taskEntity){
        return userService.del(taskEntity);
    }

    @GetMapping("/user/task/{id}")
    public TaskEntity findTaskById(@PathVariable int id) {
    return userService.getTaskById(id);
    }

    @GetMapping("/user/taskdate/{date}")
    public List<TaskEntity> findTaskByDay(@PathVariable String date) {
        return userService.getTaskByDate(LocalDate.parse(date));
    }

    @GetMapping("/user/alltasks")
    public List<TaskEntity> findAllTasks(){
        return userService.getTasks();
    }

    @PutMapping("/user/taskupdate")
    public TaskEntity updTask(@RequestBody TaskEntity taskEntity){
        return userService.put(taskEntity);
    }
}

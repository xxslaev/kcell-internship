package com.example.jwtauthreg.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @GetMapping("/admin/get")
    public String getAdmin() {
        return "Приветствую, Админ";
    }

    @GetMapping("/user/get")
    public String getUser() {
        return "Приветстую, Пользователь";
    }
}

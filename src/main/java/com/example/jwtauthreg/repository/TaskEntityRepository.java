package com.example.jwtauthreg.repository;

import com.example.jwtauthreg.Entity.TaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface TaskEntityRepository extends JpaRepository<TaskEntity, Integer> {
    List<TaskEntity> findAllByDate(LocalDate date);
}
